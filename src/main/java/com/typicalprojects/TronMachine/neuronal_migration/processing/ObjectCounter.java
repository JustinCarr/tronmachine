/*
 * (C) Copyright 2018 Justin Carrington.
 *
 *  This file is part of TronMachine.

 *  TronMachine is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TronMachine is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License
 *  along with TronMachine.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Justin Carrington
 *     Russell Taylor
 *     Kendra Taylor
 *     Erik Dent
 *     
 */
package com.typicalprojects.TronMachine.neuronal_migration.processing;

import com.typicalprojects.TronMachine.util.Logger;
import com.typicalprojects.TronMachine.util.ResultsTable;

import ij.ImagePlus;

public class ObjectCounter {

	private ImagePlus imageStack;


	
	public static int maxSize = 5000; // always mix pixel number
	public static int minSize = 200; // Can be set
	private final int threshold;
	
	public static final int NONE = 0;
	public static final int NUM_ONLY = 1;
	public static final int DOT_ONLY = 2;
	public static final int NUM_AND_DOTS = 3;
	
	public static boolean opExcludeOnEdges = false;
	
	public static boolean opCalcImgObjects = true;
	public static boolean opCalcImgSurfaces = false;
	public static boolean opCalcImgCentroids = false;
	public static boolean opCalcImgCenterOfMass = true;
	
	public static boolean opCalcStats = true;
	public static boolean opCalcSummary = false;
	
	// IN Obj Counter Options
	public static int opResultDotsSize = 10;
	public static int opResultFontSize = 20;
	public static boolean opResultWhiteNum = true;
	
	public static int opImgObjectsDispType = NONE;
	public static int opImgSurfacesDispType = NONE;
	public static int opImgCentroidsDispType = NONE;
	public static int opImgCenterOfMassDispType = NUM_AND_DOTS;
	
	public static CounterPrefs statsPrefs = new CounterPrefs();
	
	private ImagePlus objectMap = null;
	private ImagePlus surfaceMap = null;
	private ImagePlus centroidMap = null;
	private ImagePlus centerOfMassMap = null;
	
	private String summary = null;
	private ResultsTable stats = null;
		
	public ObjectCounter(ImagePlus imageStack) throws IllegalArgumentException {
		if (imageStack.getBitDepth()>16){
			throw new IllegalArgumentException();
		}

		this.imageStack = imageStack;
		this.threshold = Math.max(1, imageStack.getImageStack().getProcessor(1).getAutoThreshold());
	}

	public void run(Logger progress, NeuronProcessor nc) throws IllegalStateException {


		CounterHelper OC = new CounterHelper(this.imageStack, this.threshold, minSize, maxSize, opExcludeOnEdges, progress, nc);
		
		if (nc.isCancelled())
			return;
		
		if (opCalcImgObjects) { 
			boolean dispNum = false;
			int fontSize = 0;
			if (opImgObjectsDispType == NUM_ONLY || opImgObjectsDispType == NUM_AND_DOTS) {
				fontSize = opResultFontSize;
				dispNum = true;
			}
			this.objectMap = OC.getObjMap(dispNum, fontSize);
		}
		if (opCalcImgSurfaces){
			boolean dispNum = false;
			int fontSize = 0;
			if (opImgSurfacesDispType == NUM_ONLY || opImgSurfacesDispType == NUM_AND_DOTS) {
				fontSize = opResultFontSize;
				dispNum = true;
			}
			this.surfaceMap = OC.getSurfPixMap(dispNum, opResultWhiteNum, fontSize);
		}
		if (opCalcImgCentroids) {
			boolean dispNum = false;
			int fontSize = 0;
			if (opImgCentroidsDispType == NUM_ONLY || opImgCentroidsDispType == NUM_AND_DOTS) {
				fontSize = opResultFontSize;
				dispNum = true;
			}
			
			int dotsSize = 0;
			if (opImgCentroidsDispType == DOT_ONLY || opImgCentroidsDispType == NUM_AND_DOTS) {
				dotsSize = opResultDotsSize;
			}
			this.centroidMap = OC.getCentroidMap(dispNum, opResultWhiteNum, dotsSize, fontSize);
		}
		if (opCalcImgCenterOfMass) {
			boolean dispNum = false;
			int fontSize = 0;

			if (opImgCenterOfMassDispType == NUM_ONLY || opImgCenterOfMassDispType == NUM_AND_DOTS) {
				fontSize = opResultFontSize;
				dispNum = true;
			}
			
			int dotsSize = 0;
			if (opImgCenterOfMassDispType == DOT_ONLY || opImgCenterOfMassDispType == NUM_AND_DOTS) {
				dotsSize = opResultDotsSize;
			}
			this.centerOfMassMap = OC.getCentreOfMassMap(dispNum, opResultWhiteNum, dotsSize, fontSize);
		}


		if (opCalcStats) this.stats = OC.createStatisticsTable(statsPrefs);

		if (opCalcSummary) this.summary = OC.createSummary();
		
		OC = null;
		System.gc();
	}
	
	public ImagePlus getObjectMap() {		
		return this.objectMap;
	}
	
	public ImagePlus getSurfacesMap() {
		
		return this.surfaceMap;
	}
	
	public ImagePlus getCentroidsMap() {
		
		return this.centroidMap;
	}
	
	public ImagePlus getCOMMap() {
		
		return this.centerOfMassMap;
	}
	
	public String getSummary() {
		return this.summary;
	}
	
	public ResultsTable getStats() {
		return this.stats;
	}

}
