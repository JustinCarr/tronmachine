package com.typicalprojects.TronMachine.popup;

import java.awt.Color;
import java.awt.Font;

public interface Displayable {
	public String getListDisplayText();
	
	public Color getListDisplayColor();
	
	public Font getListDisplayFont();
}
