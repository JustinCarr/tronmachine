## Welcome

The Tron Machine is a program developed at the Erik Dent Research Lab at the University of Wisconsin-Madison. The software counts neurons and quantifies their migration in image stack files from confocal microscopy. The program analyzes neurons using common image manipulation techniques, including erosion, Gaussian blur, and thresholding.

See the [Wiki](https://bitbucket.org/JustinCarr/tronmachine/wiki/Home) for more information.

-The Dent Lab