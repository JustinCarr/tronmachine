#!/bin/bash

cd JarBuilds

quantFile=$(ls -p | grep -v / | sort -V | tail -n 1)
echo $quantFile
cd ..

curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"JarBuilds/$quantFile"


